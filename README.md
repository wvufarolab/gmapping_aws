## This package runs gmapping on AWS RoboMaker

Gmapping is a laser-based SLAM (Simultaneous Localization and Mapping) for mobile robotics. Once gmapping (http://wiki.ros.org/gmapping) is running on a cloud-based server, a remote robot can send laser scan data to the cloud, where a 2D map is built. The robot will receive localization information from the cloud.

This package uses ROS Bridge (http://wiki.ros.org/rosbridge_suite) and ROSlibpy (https://roslibpy.readthedocs.io/en/latest/) to communicate the robot with Amazon Cloud (AWS).

#### Running the package 

Sign in to the AWS RoboMaker console at https://console.aws.amazon.com/robomaker/.

---

##### Creating an environment

In the AWS RoboMaker console, expand **Development** on the left and then select **Development environments**.

Select **Create environment**.

Choose a name for your environnment (ex: gmapping) and choose the ROS distribution to be Melodic. Chose the default VPC and any subnet. Click **Create**.

In the console, create a workspace folder: 


```
#!bash

mkdir -p gmapping__ws/src
    

```
    
Clone this repository inside the src folder: 

```
#!bash

cd gmapping__ws/src
git clone https://bitbucket.org/wvufarolab/gmapping_aws.git
    
```

---

##### Building and bundle the application

Go to the workspace folder (`gmapping_ws`) and install dependencies:

```
#!bash

cd ..    
rosdep update
rosdep install --from-paths src --ignore-src -r -y
    
```

Build and bundle your application:

```
#!bash

colcon build
colcon bundle
    
```

Create an Amazon S3 bucket for your application source:

```
#!bash

aws s3 mb s3://gmapping
    
```

Copy the robot application source bundle to your Amazon S3 bucket:

```
#!bash

aws s3 cp bundle/output.tar s3://gmapping/gmapping.tar
    
```

---

##### Creating the robot application

In the AWS RoboMaker console (https://console.aws.amazon.com/robomaker/), expand **Development** on the left and then select **Robot applications**.

Select **Create robot application**.

Choose a name for your application (ex: gmapping) and choose the ROS distribution to be Melodic. Browse s3 for your X86_64 source file (`s3://gmapping/gmapping.tar`) and select **Create**.

---

##### Running the robot application


In the AWS RoboMaker console, expand **Simulations** on the left and then select **Simulation Jobs**.

Select **Create simulation job**.

Choose the duration and set the ROS version to be Melodic. Choose a role. It is recomended that you create a new role in the first run, what will set the necessary privilegies automatically.

In Networking, it a VPC must be chosen. It is also important that you have a security group that allows external communication. To create a security group go to https://console.aws.amazon.com/ec2/ and select security groups on the left. 
You will need to create two Inboud and two Outbound rules. 1) All TCP, source My IP (or the robot IP), 2) All trafic, source Anywhere.  

Mark yes for **Assign public IP** and click **Next**.

Choose your Robot application (gmapping).

The launch package is `robot_app` and the launch file is `gmapping.launch`.

In **Robot application connectivity** set both the Simulation port and the Application port to 8080 and the type to public. Click **Next**.

We have no simulation application. Select **None** and click **Next**.

Select **Create**.

---

##### Interacting with the application

Once the simulation job status is running, you can interact with your application using rviz, rqt or a terminal.

To see if all nodes are running, **Connect** to the Terminal and see the list of nodes:

```
#!bash

rosnode list
    
```

You should see:

```

/robomaker/srv
/rosapi
/rosbridge_websocket
/rosout
/rviz_1592408197591895441
/slam_gmapping
/tf2_web_republisher_node
/tf_publisher

```

You can use all command-line ROS tools in the terminal (`rostopic`, `rosparam`, `rosmsg`, etc). 

---

#### Localization and Mapping using your cloud application

To map an environemnt using your cloud application, clone the client node in your local linux machine or robot. Inside the `src` folder of your catkin workspace run:

```
#!bash

git clone https://bitbucket.org/wvufarolab/aws_mapping_meta.git

```

You may need to build  and source your workspace:

```
#!bash

cd ..
catkin_make
source devel/setup.bash

```

Run your robot or simulated robot. Launch files for a simulated Husky robot equiped with plannar Lidar can be found in the aws_mapping package. These will require the instalation of a Husky simulator (http://wiki.ros.org/husky_gazebo/Tutorials/Simulating%20Husky).
For example, if you have the Husky simulation istalled, you may want to run:

```
#!bash

roslaunch aws_mapping husky_willow.launch

```

Launch the client:

```
#!bash

roslaunch aws_mapping send_data.launch IP:=host_public_ip

```

The public IP address (`host_public_ip`) is found in the botton of your simulation job page at AWS RoboMaker.

You should be able to see the laser data and the map being contructed using RVIZ at AWS RoboMaker.

---

If you have any questions or suggestions, please e-mail <guilherme.pereira@mail.wvu.edu>

---

#### The developmet of this tutorial was supported by the Amazon Research Award (ARA) program.